import Vue from 'vue'
import VueRouter from 'vue-router'
import UserList from '@/components/UserList.vue'
import UserEdit from '@/components/UserEdit.vue'

Vue.use(VueRouter)

const Home = {render(h) {return h('h1', 'Home')} }

const routes = [
  {path: '/', component: Home},
  {path: '/users', component: UserList},
  {path: '/users/:userID', component: UserEdit},
]

export default new VueRouter({
  mode: 'history',
  routes,
})